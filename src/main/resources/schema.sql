DROP TABLE IF EXISTS POKEMON;
 
CREATE TABLE POKEMON (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  name VARCHAR(250) NOT NULL,
  url VARCHAR(250) NOT NULL
);
