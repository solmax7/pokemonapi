package ProjectPokemon;

import org.json.JSONObject;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

@SpringBootApplication
public class Demo {

    public static void main(String[] args) {
          SpringApplication.run(Demo.class, args);
    }
}
