package ProjectPokemon.Entity;

import lombok.Data;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Data
@Table(name="POKEMON")
public class Pokemon {

//    org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(Pokemon.class);
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
            private Integer id;
            @Column(name="name")
            private String name;
            @Column(name="url")
            private String url;


}
