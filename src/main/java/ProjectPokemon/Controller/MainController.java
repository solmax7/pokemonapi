package ProjectPokemon.Controller;

import ProjectPokemon.Entity.Pokemon;
import ProjectPokemon.Service.PokemonService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.net.URL;
import java.util.List;

@RestController

public class MainController {

            @Autowired
        PokemonService service;

        @GetMapping
        @RequestMapping("/index1")
        // public ResponseEntity<List<EmployeeEntity>> getAllEmployees() {
        public ModelAndView getAllEmployees() {
            ModelAndView modelAndView = new ModelAndView();

//            List<EmployeeEntity> list = service.getAllEmployees();
//            modelAndView.addObject("employees", list);
//            //return new ResponseEntity<List<EmployeeEntity>>(list, new HttpHeaders(), HttpStatus.OK);
            modelAndView.setViewName("index1");
            return modelAndView;
        }

    @GetMapping
    @RequestMapping("/index")
    // public ResponseEntity<List<EmployeeEntity>> getAllEmployees() {
    public ModelAndView API() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            Pokemon usrPost = mapper.readValue(new URL("https://pokeapi.co/api/v2/pokemon/"), Pokemon.class);
            System.out.println(usrPost);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return  null;
    }

//        @GetMapping("/{id}")
//        public ResponseEntity<EmployeeEntity> getEmployeeById(@PathVariable("id") Long id)
//                throws RecordNotFoundException {
//            EmployeeEntity entity = service.getEmployeeById(id);
//
//            return new ResponseEntity<EmployeeEntity>(entity, new HttpHeaders(), HttpStatus.OK);
//        }
//
//        @PostMapping
//        public ResponseEntity<EmployeeEntity> createOrUpdateEmployee(EmployeeEntity employee)
//                throws RecordNotFoundException {
//            EmployeeEntity updated = service.createOrUpdateEmployee(employee);
//            return new ResponseEntity<EmployeeEntity>(updated, new HttpHeaders(), HttpStatus.OK);
//        }
//
//        @DeleteMapping("/{id}")
//        public HttpStatus deleteEmployeeById(@PathVariable("id") Long id)
//                throws RecordNotFoundException {
//            service.deleteEmployeeById(id);
//            return HttpStatus.FORBIDDEN;
//        }

    }
