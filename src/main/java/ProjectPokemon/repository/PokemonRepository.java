package ProjectPokemon.repository;

import ProjectPokemon.Entity.Pokemon;
//import com.howtodoinjava.demo.model.EmployeeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PokemonRepository
        extends JpaRepository<Pokemon, Long> {

}
