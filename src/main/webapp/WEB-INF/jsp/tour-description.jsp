<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>

<head>

     <meta charset="UTF-8">
     <title>Турагенство</title>
     <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css" media="screen" type="text/css" />
     <link rel="stylesheet" href="${pageContext.request.contextPath}/css/index.css" media="screen" type="text/css" />
</head>

<body>

<ul class="menu-main">
          <li><a href="/home" class="current" ;>Главная</a></li>
          <li><a href="/tours">Все туры</a></li>
          <li><a href="/orders">Заказы</a></li>
          <li><a href="/about">О нас</a></li>
          <li><a href="/logout">Выход</a></li>

     </ul>

     	<div id="ram">
        <div><h2>${tour.name}</h2></div>

     	<div class = "dec">${tour.description}</div>



    <form action="/tour/${tour.id}" method="post" id="form-login">

        <input class="btn" type="submit" value="Заказать" />


</form>
     	</div>

<div class="footer"> &copy;Туристическая компания "TOURS" &nbsp;<span class="separator">|</span>&nbsp; <a href="http://tours.ru">TOURS</a> </div>
</body>

</html>