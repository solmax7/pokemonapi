<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Регистрация</title>
	<link rel="stylesheet" href="css/login.css" media="screen" type="text/css" />
</head>
<body>
    <div id="login">
        <div class="form-signup">
          <h1>Регистрация</h1>
          <fieldset>
          <p class="login-msg"></p>
            <form action="registration" method="post" id="registration-form">
              <input type="name"  name="name" placeholder="Введите ваше имя" required />
              <input id="user" type="login"  name="login" placeholder="Введите login" required />
              <input type="password" name="password" placeholder="Ваш сложный пароль..." required />
              <input type="submit" value="Регистрация" />
              ${errorMsg}
            </form>
            <a href="aut.html" class="flipper">Уже зарегистрированы? Войти.</a>
          </fieldset>
        </div>
        
      </div>
    </div>
  <script src="js/index.js"></script>
</div>
	<div class="footer"> &copy;Туристическая компания "TOURS" &nbsp;<span class="separator">|</span>&nbsp; <a href="http://tours.ru">TOURS</a> </div>
	</div>
</body>
<script language="JavaScript" type="text/javascript" src="css/js/jquery-3.3.1.min.js"></script>
<script language="JavaScript" type="text/javascript" src="css/js/registration.js"></script>
</html>