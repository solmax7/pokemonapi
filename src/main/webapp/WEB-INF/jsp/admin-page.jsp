<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>

<head>

     <meta charset="UTF-8">
     <title>Турагенство</title>
     <link rel="stylesheet" href="css/index.css" media="screen" type="text/css" />
</head>


<body>

       <ul class="menu-main">
          <li><a href="/home" class="current" ;>Главная</a></li>
          <li><a href="/tours">Все туры</a></li>
          <li><a href="/orders">Заказы</a></li>
          <li><a href="/about">О нас</a></li>
          <li><a href="/logout">Выход</a></li>

     </ul>

<div id="ram">
     	<div id ="form-admin">
              <h1>Добавить новый тур</h1>
              <fieldset>
                <form action="/addTour" method="post" id="form-login">
                  <input input id="name" name="name" type="text" placeholder="Название тура" required />
                   <input input id="description" name="description" type="text" placeholder="Описание" required />
                  <input id="price" name="price" placeholder="Цена" required />
                   <input id="date" type = date name="date" placeholder="Дата" required />
                  <input type="submit" value="Записать" />
                  ${errorMsg}
                </form>



              </fieldset>
            </div>

     	</div>

     	<div>

     	${tour.description}


     	</div>

</div>
	<div class="footer"> &copy;Туристическая компания "TOURS" &nbsp;<span class="separator">|</span>&nbsp; <a href="http://tours.ru">TOURS</a> </div>
	</div>
</body>
</html>



