<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>

<head>

     <meta charset="UTF-8">
     <title>Турагенство</title>
     <link rel="stylesheet" href="${pageContext.request.contextPath}/css/index.css" media="screen" type="text/css" />
</head>

<body>

<ul class="menu-main">
          <li><a href="/home" class="current" ;>Главная</a></li>
          <li><a href="/tours">Все туры</a></li>
          <li><a href="/orders">Заказы</a></li>
          <li><a href="/contacts">О нас</a></li>
          <li><a href="/logout">Выход</a></li>

     </ul>

     	<div id="ram">
        <div><h2>${tour.name}</h2></div>

     	<div class = "dec">Главная задача туристического портала Паблик Трэвел - собрать все туры от туристических агентств и предоставить туристу выбор. Ежедневно мы собираем все самые выгодные спецпредложения от турфирм Ижевска. Вам больше не нужно подписываться на множество туристических групп в Одноклассниках, ВКонтакте, Facebook, добавлять в закладки понравившиеся сайты с предложениями по турам в Ижевске, а также обходить десятки турфирм - мы собираем все туры от турфирм Ижевска в одном месте - на портале Паблик Трэвел Ижевск.</div>

     	</div>

<div class="footer"> &copy;Туристическая компания "TOURS" &nbsp;<span class="separator">|</span>&nbsp; <a href="http://tours.ru">TOURS</a> </div>
</body>

</html>